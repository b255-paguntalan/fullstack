import { Fragment, useEffect } from 'react';
import './App.css';
import React from 'react';
import { Container } from 'react-bootstrap';
import { BrowserRouter as Router} from 'react-router-dom';
import { Route, Routes, Navigate } from 'react-router-dom';
import AppNavbar from './components/AppNavbar';
import Register from './pages/Register';
import Login from './pages/Login';
import Home from './pages/home';
import Courses from './pages/Courses';
import Logout from './pages/Logout';
import Error from './pages/Error';
import { useState } from 'react';
import { UserProvider } from './UserContext';
import CourseView from './components/CourseView';

export default function App() {

  // State hook for the user state that's defined here for a gloabal scope
  // Initialized as an object with properties from the local storage
  // This will be used to store the user information and will be used for validating if a user is logged in on the app or not

  const [user, setUser] = useState({
    // email: localStorage.getItem("email")

    id: null,
    isAdmin: null
  })

  // Function for clearing localstorage on logout
  const unsetUser = () => {
    localStorage.clear()
  }

  // Used to check if user information is properly stored pon login
  useEffect(() => {
    console.log(user)
    console.log(localStorage)
  }, [user])

  return (
    <UserProvider value={{user, setUser, unsetUser}}>
      <Router>
        <Container fluid>
        
          <AppNavbar/>

          <Routes>
            <Route path='/' element={<Home/>} />
            <Route path='/register' element={<Register/>} />
            <Route path='/login' element={<Login/>} />
            <Route path='/courses' element={<Courses/>} />
            <Route path='/courseView/:courseId' element={<CourseView/>} />
            <Route path='/logout' element={<Logout/>} />
            <Route path='*' element={<Error/>} />
          </Routes>

        </Container>
      </Router>
    </UserProvider>

  );
}
