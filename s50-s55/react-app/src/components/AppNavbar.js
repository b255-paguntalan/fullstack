import Container from 'react-bootstrap/Container';
import { Fragment, useContext, useState } from 'react';
import Navbar from 'react-bootstrap/Navbar';
import { Nav } from 'react-bootstrap';
import { Link, NavLink } from 'react-router-dom';
import React from 'react';
import UserContext from '../UserContext';

export default function AppNavbar(){

	// State to store the user information stored in the login page.
	const { user } = useContext(UserContext);


    return(
        <Navbar bg="light" expand="md">
			<Container fluid>
					<Navbar.Brand as={Link} to="/">Zuitt Booking</Navbar.Brand>
					<Navbar.Toggle aria-controls="basic-navbar-nav"/>
					<Navbar.Collapse id="basic-navbar-nav">
					    <Nav className="mr-auto">
					    <Nav.Link as={NavLink} to="/">Home</Nav.Link>

					    <Nav.Link as={NavLink} to="/courses" exact>Courses</Nav.Link>

						{(user.id !== null) ? 
						<Nav.Link as={NavLink} to="/logout" exact>Logout</Nav.Link>
						:
						<React.Fragment>
						<Nav.Link as={NavLink} to="/login" exact>Login</Nav.Link>
						<Nav.Link as={NavLink} to="/register" exact>Register</Nav.Link>
						</React.Fragment>
						}
						
					    </Nav>
				</Navbar.Collapse>
			</Container>
		</Navbar>
    ) 
}