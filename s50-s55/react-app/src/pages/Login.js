import { Form, Button } from "react-bootstrap";
import { useEffect, useState, useContext } from "react";
import { Navigate } from "react-router-dom";
import UserContext from "../UserContext";
import { type } from "@testing-library/user-event/dist/type";
import Swal from 'sweetalert2';

export default function Login(props) {

    // Allows us to consume the User context object and it's preoperties to use for user validation
    const {user, setUser} = useContext(UserContext);

    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [isActive, setIsActive] = useState(false);

    function loginUser(e) {
        e.preventDefault();

        // Process a fetch request to the corresponding backend API
        // The header information "Content-Type" is used to specify that the information being sent to the backend will be sent in the form of JSON
        // The fetch request will communicate with our backend application providing it with a stringified JSON
        fetch('http://localhost:4000/users/login', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email:email,
                password: password
            })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data)

            // If no user information found, the "access" property will not be available and will return undefined 
            if(typeof data.access !== "undefined") {
                localStorage.setItem('token', data.access);
                retrieveUserDetails(data.access)

                Swal.fire({
                    title: "Login Successful",
                    icon: "success",
                    text: "Welcome to Zuitt!"
                })
            }
            else {
                Swal.fire({
                    title: "Authentication Failed",
                    icon: "error",
                    text: "Check your login details and try again."
                })
            }
        })

        // Set the email of the authenticated user in the local storage
        // Syntax

        /* localStorage.setItem("email", email) */

        // Set the global user state to have properties obtained from the localstorage

        // Though access to the user information can be done via the localStorage this is necessary to update the user state which will help update the App component and rerender it to avoid refreshing the page
        // When state change components are rerendered and the AppNavBar components will be updated based on the user credentials

        /* setUser({
            email: localStorage.getItem("email")
        }) */

        // Clear input fields after submission
        setEmail("");
        setPassword("");
        alert("Successfully logged in!");
    }

    const retrieveUserDetails = (token) => {

        // The token will be sent as part of the requests header information
        fetch('http://localhost:4000/users/details', {
            header: {
                Authorization: `Bearer ${token}`
            }
        })
        .then(res => res.json())
        .then(data => {
            console.log(data)

            setUser({
                id: data._id,
                isAdmin: data.isAdmin
            })

        })
    }

    useEffect( () => {
        if(email !== "" && password !== "") {
            setIsActive(true)
        }
    }, [email, password]);
        
    return (

        (user.id !== null) ?
            <Navigate to="/courses"/>
            :

            <Form onSubmit={(e) => loginUser(e)}>

                <h1>Login</h1>
    
                <Form.Group controlId="userEmail">
                    <Form.Label>Email address</Form.Label>
                    <Form.Control 
                        type="email" 
                        placeholder="Enter email" 
                        value={email}
                        onChange={e => setEmail(e.target.value)}
                        required
                    />
                    <Form.Text className="text-muted">
                        We'll never share your email with anyone else.
                    </Form.Text>
                </Form.Group>
    
                <Form.Group controlId="password">
                    <Form.Label>Password</Form.Label>
                    <Form.Control 
                        type="password" 
                        placeholder="Password" 
                        value={password}
                        onChange={e => setPassword(e.target.value)}
                        required
                    />
                </Form.Group><br/>
    
                {isActive ? 
    
                    <Button variant="primary" type="submit" id="submitBtn">
                        Login
                    </Button>
                    :
                    <Button variant="primary" type="submit" id="submitBtn" disabled>
                        Login
                    </Button>
                }
            </Form>

        
    )

}