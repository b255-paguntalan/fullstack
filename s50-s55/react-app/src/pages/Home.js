import { Fragment } from 'react';
import Banner from '../components/banner';
import Highlights from '../components/Highlights';

export default function Home() {
    return (
        <Fragment>
            <Banner />
            <Highlights />
        </Fragment>
    )
}