import { Form, Button } from "react-bootstrap";
import { useEffect, useState, Navigate, useContext } from "react";
import UserContext from "../UserContext";
import Swal from "sweetalert2";

export default function Register() {

    // State hook to store the values of the input fields
    const [firstName, setFirstName] = useState("")
    const [lastName, setLastName] = useState("")
    const [email, setEmail] = useState("");
    const [mobileNo, setMobileNo] = useState("");
    const [password1, setPassword1] = useState("");
    const [password2, setPassword2] = useState("");

    // State to determine whether submit button is enabled or not
    const [isActive, setIsActive] = useState("");

    const {user, setUser} = useContext(UserContext);

    console.log(email);
    console.log(password1);
    console.log(password2);
 
    function registerUser(e) {
        // Prevents page redirection via form submission
        e.preventDefault();

        fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                "firstName" : firstName,
                "lastName" : lastName,
                "email" : email,
                "mobileNo" : mobileNo,
                "password" : password1
            })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data)
    
            if(data === true) {
    
                Swal.fire({
                    title: "Registration Successful",
                    icon: "success",
                    text: "Welcome to Zuitt!"
                }).then(() => {
                    window.location.href = "/login";
                });
            }
            else {
                Swal.fire({
                    title: "Duplicate email found",
                    icon: "error",
                    text: "Please provide a different email."
                })
            }
        })
    
        // Clear input fields
        setFirstName("");
        setLastName("");
        setEmail("");
        setMobileNo("");
        setPassword1("");
        setPassword2("");
    }
    
    useEffect(() => {
        // Validation to enable submit button when all fields are populated and both passwords match

        if((firstName !== "" && lastName !== "" && email !== "" && mobileNo !== "" && password1 !== "" && password2 !== "") && (password1 === password2) && (mobileNo.length === 11)) {
            setIsActive(true)
        }

    }, [firstName, lastName, email, mobileNo, password1, password2])

    return (

    (user.id !== null) ?
        <Navigate to="/courses"/>
        :

        <Form onSubmit={(e) => registerUser(e)}>

            <Form.Group controlId="firstName">
                <Form.Label>First Name</Form.Label>
                <Form.Control 
                    type="text" 
                    placeholder="Enter first name" 
                    value={firstName}
                    onChange={e => setFirstName(e.target.value)}
                    required
                />
            </Form.Group>

            <Form.Group controlId="lastName">
                <Form.Label>Last Name</Form.Label>
                <Form.Control 
                    type="text" 
                    placeholder="Enter last name" 
                    value={lastName}
                    onChange={e => setLastName(e.target.value)}
                    required
                />
            </Form.Group>

            <Form.Group controlId="userEmail">
                <Form.Label>Email address</Form.Label>
                <Form.Control 
                    type="email" 
                    placeholder="Enter email" 
                    value={email}
                    onChange={e => setEmail(e.target.value)}
                    required
                />
                <Form.Text className="text-muted">
                    We'll never share your email with anyone else.
                </Form.Text>
            </Form.Group>

            <Form.Group controlId="mobileNumber">
                <Form.Label>Mobile Number</Form.Label>
                <Form.Control 
                    type="number" 
                    placeholder="Enter mobile number (09XXXXXXXXX)" 
                    value={mobileNo}
                    onChange={e => setMobileNo(e.target.value)}
                    required
                />
            </Form.Group>

            <Form.Group controlId="password1">
                <Form.Label>Password</Form.Label>
                <Form.Control 
                    type="password" 
                    placeholder="Password" 
                    value={password1}
                    onChange={e => setPassword1(e.target.value)}
                    required
                />
            </Form.Group>

            <Form.Group controlId="password2">
                <Form.Label>Verify Password</Form.Label>
                <Form.Control 
                    type="password" 
                    placeholder="Verify Password" 
                    value={password2}
                    onChange={e => setPassword2(e.target.value)}
                    required
                />
            </Form.Group>

            {isActive ? 

                <Button variant="primary" type="submit" id="submitBtn">
                    Submit
                </Button>
                :
                <Button variant="primary" type="submit" id="submitBtn" disabled>
                    Submit
                </Button>
            }
        </Form>
    )
}