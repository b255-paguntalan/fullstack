import React from 'react';
import ReactDOM from 'react-dom/client';
import 'bootstrap/dist/css/bootstrap.min.css'
import App from './App';



// createRoot - assigns element to tbe used by React with its Virtual DOM
const root = ReactDOM.createRoot(document.getElementById('root'));

// Render displays the react element/components into the root 

// app component is our mother component, this is the component that we use as entry point and where we can render all other components

// <React.StrictMode> - component from react that manages future or possible conflicts. It allows us to extend or expand certain error messages

root.render(
  <React.StrictMode>
    <App/>
  </React.StrictMode>
);

/* const name = 'John Smith'
const user = {
  firstName: 'Jane',
  lastName: 'Smith'
}

function formatName(user){
  return user.firstName + ' ' + user.lastName;
}

const element = <h1>Hello, {formatName(user)}</h1>

const root = ReactDOM.createRoot(document.getElementById('root'));

root.render(element) */

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals

