let collection = [];

// Write the queue functions below.

function print() {
    return collection;
}

function enqueue(e) {
collection.push(e);
return collection;
}

function dequeue(e) {
collection.shift(e);
return collection;
}

function front() {
return collection[0];
}

function size() {
return collection.length;
}

function isEmpty() {
    if(collection.length === 0) {
        return true
    }
    else{
        return false
    }
}

module.exports = {
    collection,
    print,
    enqueue,
    dequeue,
    front,
    size,
    isEmpty
};