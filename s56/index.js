// Number 1.

// Check first whether the letter is a single character.
// If letter is a single character, count how many times a letter has occurred in a given sentence then return count.
// If letter is invalid, return undefined.

function countLetter(letter, sentence) {
    if (typeof letter !== 'string' || letter.length !== 1) {
        return undefined;
    }

    let count = 0;
    for (let i = 0; i < sentence.length; i++) {
        if (sentence[i] === letter) {
        count++;
        }
    }
    return count;
}

const result1 = countLetter("l", "little");
console.log(result1)



// Number 2.

// An isogram is a word where there are no repeating letters.
// The function should disregard text casing before doing anything else.
// If the function finds a repeating letter, return false. Otherwise, return true.

function isIsogram(text) {
    text = text.toLowerCase();

    for (let i = 0; i < text.length; i++) {
        if (text.indexOf(text[i]) !== i) {
        return false;
        }
    }

    return true;
}
  
const result2 = isIsogram("Uncopyrightable");
console.log(result2)



// Number 3.

// Return undefined for people aged below 13.
// Return the discounted price (rounded off) for students aged 13 to 21 and senior citizens. (20% discount)
// Return the rounded off price for people aged 22 to 64.
// The returned value should be a string.

function purchase(age, price) {
    if (age < 13) {
        return undefined;
    }

    if (age >= 13 && age <= 21 || age >= 65) {
        const discountedPrice = Math.round(price * 0.8);
        return discountedPrice.toString();
    }

    const roundedPrice = Math.round(price);
    return roundedPrice.toString();
}

const result3 = purchase(25, 55.6);
console.log(result3); 
  


// Number 4.

// Find categories that has no more stocks.
// The hot categories must be unique; no repeating categories.
// The passed items array from the test are the following:
// { id: 'tltry001', name: 'soap', stocks: 14, category: 'toiletries' }
// { id: 'tltry002', name: 'shampoo', stocks: 8, category: 'toiletries' }
// { id: 'tltry003', name: 'tissues', stocks: 0, category: 'toiletries' }
// { id: 'gdgt001', name: 'phone', stocks: 0, category: 'gadgets' }
// { id: 'gdgt002', name: 'monitor', stocks: 0, category: 'gadgets' }
// The expected output after processing the items array is ['toiletries', 'gadgets'].
// Only putting return ['toiletries', 'gadgets'] will not be counted as a passing test during manual checking of codes.

function findHotCategories(items) {
    const hotCategories = [];
  
    items.forEach(item => {
        if (item.stocks === 0) {
        
        if (!hotCategories.includes(item.category)) {
            hotCategories.push(item.category);
        }
        }
    });

    return hotCategories;
}

const items = [

    { id: 'tltry001', name: 'soap', stocks: 14, category: 'toiletries' },
    { id: 'tltry002', name: 'shampoo', stocks: 8, category: 'toiletries' },
    { id: 'tltry003', name: 'tissues', stocks: 0, category: 'toiletries' },
    { id: 'gdgt001', name: 'phone', stocks: 0, category: 'gadgets' },
    { id: 'gdgt002', name: 'monitor', stocks: 0, category: 'gadgets' }

    ];

console.log(findHotCategories(items));
  


// Number 5.

// Find voters who voted for both candidate A and candidate B.
// The passed values from the test are the following:
// candidateA: ['LIWf1l', 'V2hjZH', 'rDmZns', 'PvaRBI', 'i7Xw6C', 'NPhm2m']
// candidateB: ['kcUtuu', 'LLeUTl', 'r04Zsl', '84EqYo', 'V2hjZH', 'LIWf1l']
// The expected output after processing the candidates array is ['LIWf1l', 'V2hjZH'].
// Only putting return ['LIWf1l', 'V2hjZH'] will not be counted as a passing test during manual checking of codes.

function findFlyingVoters(candidateA, candidateB) {
    const flyingVoters = candidateA.filter((voter) => candidateB.includes(voter));
    return flyingVoters;
}
  
const candidateA = ['LIWf1l', 'V2hjZH', 'rDmZns', 'PvaRBI', 'i7Xw6C', 'NPhm2m'];
const candidateB = ['kcUtuu', 'LLeUTl', 'r04Zsl', '84EqYo', 'V2hjZH', 'LIWf1l'];

const flyingVoters = findFlyingVoters(candidateA, candidateB);

console.log(flyingVoters);
  
  

module.exports = {
    countLetter,
    isIsogram,
    purchase,
    findHotCategories,
    findFlyingVoters
};
